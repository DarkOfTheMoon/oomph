namespace oomph
{



//===========================================================================
/// Helper namespace for pseudo-elastic elements
//===========================================================================
namespace PseudoSolidHelper
{

 /// \short Static variable to hold the default value for the pseudo-solid's
 /// inertia parameter Lambda^2.
 double Zero=0.0;

}



}
